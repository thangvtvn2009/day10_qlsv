package com.example.bai10_ontap.fragments;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.example.bai10_ontap.R;
import com.example.bai10_ontap.data.DataProvider;
import com.example.bai10_ontap.data.DataProviderInterface;
import com.example.bai10_ontap.object.Students;

public class NewFragment extends BaseFragment implements OnClickListener {

	View btnAdd, btnRest;
	TextView txtDob;
	EditText txtName, txtNote;
	ImageView img;

	Context context;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_new, container, false);
		btnAdd = view.findViewById(R.id.btnAdd);
		btnRest = view.findViewById(R.id.btnReset);
		txtName = (EditText) view.findViewById(R.id.txtName);
		txtDob = (TextView) view.findViewById(R.id.txtDoB);
		txtDob.setText(GetCurrentDate());
		txtNote = (EditText) view.findViewById(R.id.txtNote);
		img = (ImageView) view.findViewById(R.id.done);
	
		txtName.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				img.setVisibility(View.VISIBLE);
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {

				
			}
		});
		btnAdd.setOnClickListener(this);
		btnRest.setOnClickListener(this);
		txtDob.setOnClickListener(this);
		return view;

	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.btnAdd:
			String name = txtName.getText().toString();
			String DoB = txtDob.getText().toString();
			String Note = txtNote.getText().toString();
			if (Note.isEmpty())
				Note = null;

			if (name.isEmpty())
				txtName.setError(getString(R.string.err1));

			else {
				Students student = new Students();
				student.setName(name);
				student.setDoB(DoB);
				student.setNote(Note);
				DataProviderInterface data = new DataProvider();
				if (data.Insert(student)) {

					Toast.makeText(getActivity(), "Add susscess",
							Toast.LENGTH_SHORT).show();
					ClearAll();
					txtDob.setText(GetCurrentDate());
					img.setVisibility(View.INVISIBLE);
				} else {
					Toast.makeText(getActivity(), "fail", Toast.LENGTH_SHORT)
							.show();
				}
			}

			break;

		case R.id.btnReset:
			ClearAll();
			break;

		case R.id.txtDoB:

			ShowDateDialog();

			break;

		}

	}

	private void ShowDateDialog() {

		OnDateSetListener setDate = new OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {

				txtDob.setText(dayOfMonth + "/" + monthOfYear + "/" + year);

			}
		};

		String CurrentDate = txtDob.getText().toString();
		String myDateArray[] = CurrentDate.split("/");
		int day = Integer.parseInt(myDateArray[0]);
		int month = Integer.parseInt(myDateArray[1]);
		int year = Integer.parseInt(myDateArray[2]);

		DatePickerDialog dialog = new DatePickerDialog(getActivity(), setDate,
				year, month, day);
		dialog.show();

	}

	private String GetCurrentDate() {
		Date date = new Date();
		String DateFormat = "dd/MM/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormat);
		return sdf.format(date);
	}

	private void ClearAll() {
		txtName.setText(null);
		txtDob.setText(null);
		txtNote.setText(null);
	}
}
