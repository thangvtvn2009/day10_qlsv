package com.example.bai10_ontap.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.R.integer;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bai10_ontap.R;
import com.example.bai10_ontap.Adapter.Adapter;
import com.example.bai10_ontap.constant.FragmentId;
import com.example.bai10_ontap.data.DataProvider;
import com.example.bai10_ontap.data.DataProviderInterface;
import com.example.bai10_ontap.object.Students;

public class HomeFragment extends BaseFragment implements OnClickListener {
	Button btnNew, btnDel, btnselect;
	CheckBox cb = null;
	ArrayList<Students> stlist;
	ListView lv;
	Adapter adapter;
	DataProviderInterface data = new DataProvider();
	static boolean visibleCB;
	ArrayList<Students> listcheck = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_home, container, false);

		Students st;
		// createData();
		stlist = data.getData();
		
		visibleCB = false;
		adapter = new Adapter(stlist, getActivity());
		lv = (ListView) view.findViewById(R.id.listView1);
		lv.setAdapter(adapter);
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				showDialog(stlist.get(arg2).getName(), arg2);

				return true;
			}
		});

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Bundle bundle = new Bundle();
				bundle.putSerializable("student", stlist.get(position));
				addFrgment(FragmentId.DETAIL, bundle);

			}
		});

		lv.setOnScrollListener(new OnScrollListener() {
			int count = 0;
			int mfirst = 0;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if ((scrollState == SCROLL_STATE_IDLE) && (visibleCB == true)) {
					if ((Adapter.getCheckedList() != null))
						for (int i = 0; i < view.getChildCount(); i++) {
							cb = (CheckBox) view.getChildAt(i).findViewById(
									R.id.checkBox1);
							if (Adapter.getCheckedList().contains(
									stlist.get(view.getFirstVisiblePosition()
											+ i)))
								cb.setChecked(true);
							else
								cb.setChecked(false);
						}
				}

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if ((Adapter.getCheckedList() != null))
					for (int i = 0; i < view.getChildCount(); i++) {
						cb = (CheckBox) view.getChildAt(i).findViewById(
								R.id.checkBox1);
						if (Adapter.getCheckedList().contains(
								stlist.get(view.getFirstVisiblePosition() + i)))
							cb.setChecked(true);
						else
							cb.setChecked(false);
					}
			}
		});
		btnNew = (Button) view.findViewById(R.id.New);
		btnNew.setOnClickListener(this);
		btnDel = (Button) view.findViewById(R.id.Del);
		btnDel.setOnClickListener(this);

		return view;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.New:
			addFrgment(FragmentId.NEW, null);
			break;

		case R.id.Del:
			View view;

			if (visibleCB == false) {
				for (int i = 0; i < lv.getChildCount(); i++) {
					view = lv.getChildAt(i);
					cb = (CheckBox) view.findViewById(R.id.checkBox1);
					cb.setVisibility(View.VISIBLE);

				}

				visibleCB = true;
			} else {
				visibleCB = false;
				if (Adapter.getCheckedList() != null) {
					stlist.removeAll(Adapter.getCheckedList());
					adapter.notifyDataSetChanged();
					data.Update(stlist);
					Adapter.clearCheckedList();
				}
				for (int i = 0; i < lv.getChildCount(); i++) {
					view = lv.getChildAt(i);
					cb = (CheckBox) view.findViewById(R.id.checkBox1);
					cb.setChecked(false);
					cb.setVisibility(View.INVISIBLE);

				}
			}

			break;
		default:
			break;
		}

	}

	private void showDialog(String title, final int pos) {

		AlertDialog.Builder builder = new Builder(getActivity());

		builder.setTitle("Delete!!!");
		builder.setMessage("Want to delete " + title + " ?");
		builder.setNegativeButton("Thoat",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();

					}
				});
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				stlist.remove(pos);
				data.Update(stlist);
				adapter.notifyDataSetChanged();
			}
		});

		builder.show();

	}

	public void createData() {
		ArrayList<Students> temps = new ArrayList<Students>();
		Students temp;
		for (int i = 0; i < 27; i++) {
			temp = new Students();
			temp.setName("Student " + i);
			temp.setDoB("0" + i + "/11/1990");
			temp.setNote("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" + i + i + i + i + i
					+ i + i + i + i + i + i);
			temps.add(temp);
		}
		data.Update(temps);
	}

	static public boolean getDelStated() {
		return visibleCB;
	}
}
