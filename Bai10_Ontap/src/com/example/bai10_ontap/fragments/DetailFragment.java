package com.example.bai10_ontap.fragments;

import com.example.bai10_ontap.R;
import com.example.bai10_ontap.object.Students;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DetailFragment extends BaseFragment {

	TextView name, birthday, note;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater
				.inflate(R.layout.fragment_detail, container, false);
		Bundle bundle = getArguments();
		Students studentInfo = (Students) bundle.getSerializable("student");
		name = (TextView) view.findViewById(R.id.detailName);
		birthday = (TextView) view.findViewById(R.id.detailBirth);
		note = (TextView) view.findViewById(R.id.detailNote);
		name.setText(studentInfo.getName());
		birthday.setText(studentInfo.getDoB());
		if (studentInfo.getNote().compareTo("null")==0)
			note.setText("");
		else
			note.setText(studentInfo.getNote());
		return view;
	}

}
