package com.example.bai10_ontap.data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

import com.example.bai10_ontap.MainActivity;
import com.example.bai10_ontap.object.Students;

public class DataProvider implements DataProviderInterface {

	@Override
	public boolean Insert(Students students) {
		File file=MainActivity.getFile();
		//MainActivity:file;
		try {
			
			if(!file.exists()){
				file.createNewFile();
			}
			BufferedWriter bw = new BufferedWriter(new FileWriter(file,true));
			bw.write(students.getName()+"\t"+students.getDoB()+"\t"+students.getNote()+"\r\n");
			bw.close();
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public ArrayList<Students> getData() {
		File file=MainActivity.getFile();
		try {
			if(!file.exists()){
				file.createNewFile();
			}
			
			Scanner sc = new Scanner(file);
			ArrayList<Students> arr = new ArrayList<Students>();
			while(sc.hasNextLine()){
				String line = sc.nextLine();
				String s[] = line.split("\t");
				if(s.length >= 2){
					Students students = new Students();
					students.setName(s[0]);
					students.setDoB(s[1]);
					students.setNote(s[2]);
					arr.add(students);
				}
			}
			
			sc.close();
			return arr;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void Update(ArrayList<Students> List) {
		File file=MainActivity.getFile();
		
		try {
			
			if(!file.exists()){
				file.createNewFile();
			}
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			for(int i=0;i<List.size();i++){
			bw.write(List.get(i).getName()+"\t"+List.get(i).getDoB()+"\t"+List.get(i).getNote()+"\r\n");
			}
			bw.close();

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	
	}

}
