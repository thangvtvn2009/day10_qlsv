package com.example.bai10_ontap.data;

import java.io.File;
import java.util.ArrayList;

import com.example.bai10_ontap.object.Students;

public interface DataProviderInterface {
	
	public boolean Insert (Students students);
	public ArrayList<Students> getData();
	public void Update(ArrayList<Students> List);

}
