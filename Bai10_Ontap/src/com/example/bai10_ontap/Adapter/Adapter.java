package com.example.bai10_ontap.Adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.bai10_ontap.R;
import com.example.bai10_ontap.fragments.HomeFragment;
import com.example.bai10_ontap.object.Students;

import android.R.color;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.opengl.Visibility;
import android.text.StaticLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class Adapter extends BaseAdapter {
	Context context;
	HomeFragment homeFragment;
	ArrayList<Students> data;
	
	static private ArrayList<Students> listchecked;
	static private ArrayList<Integer> saveListCheck;

	public Adapter(ArrayList<Students> data, Context context) {
		this.context = context;
		this.data = data;
		listchecked = new ArrayList<Students>();
		saveListCheck = new ArrayList<Integer>();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(data==null)
			return 0;
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TextView name;
		// TextView birth;
		Holder holder = null;
		LayoutInflater inf = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = inf.inflate(R.layout.view, parent, false);
			holder = new Holder();
			holder.name = (TextView) convertView.findViewById(R.id.name);
			holder.birth = (TextView) convertView.findViewById(R.id.birth);
			holder.cb = (CheckBox) convertView.findViewById(R.id.checkBox1);

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.name.setText(position +".\n" + data.get(position).getName());
		holder.birth.setText(data.get(position).getDoB());
		if(HomeFragment.getDelStated()==false){
			holder.cb.setVisibility(View.INVISIBLE);
			holder.cb.setChecked(false);
		}
		else
			holder.cb.setVisibility(View.VISIBLE);

		final Students temp = data.get(position);
		final int eee = position;
		holder.cb
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked == true)
						{
						
							listchecked.add(temp);					
						}
						else
						{
							listchecked.remove(temp);
						
						}
					}
				});
		if (position % 2 == 0) {

			convertView.setBackgroundColor(context.getResources().getColor(
					R.color.listviewBG));
		} else
			convertView.setBackgroundColor(context.getResources().getColor(
					R.color.listviewBG2));
		return convertView;

	}

	static public ArrayList<Students> getCheckedList() {
		if(listchecked.isEmpty())
			return null;
		return listchecked;
	}
	static public void clearCheckedList(){
		listchecked.clear();

	}
	static public void ShowCheckedList(){
		System.out.println(listchecked.size());
	}
	public class Holder {
		TextView name;
		TextView birth;
		CheckBox cb;
	}
}
